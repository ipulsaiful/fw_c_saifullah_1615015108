<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\admin;
use App\pengguna;

class admincontroller extends Controller
{
     public function awal(){
    	$admin=admin::all();
    	return view('admin.app',compact('admin'));
    }
    public function tambah(){
    	$user = pengguna::all(['username','id'])->pluck('username','id');
        return view('admin.tambah', compact('user'));
    }
    public function simpan(Request $input){
    	
        $this->validate($input,array( 
        
        'nama' => 'required',
        'notlp' => 'required',
        'email' => 'required|email',
        'alamat' => 'required',
        'pengguna_id' => 'required|integer',
        ));

        $admin= new admin();
    	$admin->nama=$input->nama;
    	$admin->notlp=$input->notlp;
    	$admin->email=$input->email;
    	$admin->alamat=$input->alamat;
        $admin->pengguna_id=$input->pengguna_id;
    	$status=$admin->save();
    	return redirect('admin');
    }
    public function edit($id){
    	$admin=admin::find($id);
        $user = pengguna::all(['username','id'])->pluck('username','id');
    	return view('admin.edit', compact('user'))->with(array('admin'=>$admin));
    }
    public function update ($id, Request $input){
    	

        $admin=admin::find($id);
    	$admin->nama=$input->nama;
    	$admin->notlp=$input->notlp;
    	$admin->email=$input->email;
    	$admin->alamat=$input->alamat;
        $admin->pengguna_id=$input->pengguna_id;
    	$status=$admin->save();
    	return redirect('admin')->with(['status'=>$status]);
    }
    public function hapus($id){
    	$admin=admin::find($id);
    	$admin->delete();
    	return redirect('admin');
    }
}