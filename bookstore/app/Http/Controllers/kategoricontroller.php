<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\kategori;

class kategoricontroller extends Controller
{
    public function awal(){
      $kategori=kategori::all();
      return view('kategori.app',compact('kategori'));
    }
    public function tambah(){
      $kategori = kategori::all();
      return view('kategori.tambah', compact('kategori'));
    }
    public function simpan(Request $input){

      $this->validate($input,array( 
        
        'deskripsi' => 'required',
      ));
      
      $kategori  = new kategori();
      $kategori->deskripsi = $input->deskripsi;
      $kategori->save();
      return redirect('kategori');
    }
    public function edit($id){
      $kategori = kategori::find($id);
      return view('kategori.edit')->with(array('kategori'=>$kategori));
    }

    public function update($id, Request $input){
      $kategori = kategori::find($id);
      $kategori->deskripsi=$input->deskripsi;
      $kategori->save();
      return redirect('kategori');
    }
    public function hapus($id){
      $kategori = kategori::find($id);
      $kategori->delete();
      return redirect('kategori');
    }
}
