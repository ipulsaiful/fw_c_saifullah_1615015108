<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buku extends Model
{
    protected $table = 'buku';
    protected $fillable = ['judul','penerbit','tanggal','kategori_id'];

    public function kategori(){
		return $this->belongsTo('App\kategori');    	
    }
    public function penulis(){
    	return $this->belongsToMany('App\penulis');
    }

}
