<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pengguna extends Model
{
    protected $table = 'pengguna';
    protected $fillable = ['username','password'];
    
    public function pembeli()
    {
		return $this->hasOne('App\pembeli');
	}
	public function admin()
    {
		return $this->hasOne('App\admin');
	}
}
