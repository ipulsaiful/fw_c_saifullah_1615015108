@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		<strong>Data Penulis</strong>
		<div class="pull-right">
			Tambah Data <a href="{{ url('tambah/penulis')}}"><button class="btn btn-primary">Tambah</button></a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
				</tr>
				@foreach($penulis as $Penulis)
					
				<tr>
					<td >{{ $Penulis->nama }}</td>
					<td >{{ $Penulis->notlp}}</td>
					<td >{{ $Penulis->email }}</td>
					<td >{{ $Penulis->alamat}}</td>
					<td >
					
					<a href="{{url('penulis/edit/'.$Penulis->id)}}" class="btn btn-success btn-xs">edit</a>
					<a href="{{url('penulis/hapus/'.$Penulis->id)}}" class="btn btn-danger btn-xs">delete</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection

